package pipez;

import pipez.core.Block;
import pipez.core.Pipe;

import static pipez.core.SpecialBlocks.*;

/**
 * Returns only Blocks which have at least one value which has the given string.
 * e.g. for a given CSV file, only lines which contain the given string are passed through.
 * 
 * Also includes an inverse-mode, which does not pass through Blocks that have at least
 * one value which has the given string.
 * 
 * @author whwong
 *
 */
public class ValueMatchPipe implements Pipe {

	@Override
	public String getName() {
		return "Value Match";
	}

	private String toMatch = null;
	private boolean inverse = false, ignoreCase = false;
	
	private ValueMatchPipe(String toMatch, boolean inverse, boolean ignoreCase) {
		this.toMatch = toMatch;
		this.inverse = inverse;
		this.ignoreCase = ignoreCase;
	}
	
	/*	
	 * All Match
	 */
	public static ValueMatchPipe create(String toMatch) {
		return new ValueMatchPipe(toMatch, false, false);
	}
	
	/*
	 * Match Ignore Case
	 */
	public static ValueMatchPipe createIgnoreCase(String toMatch) {
		return new ValueMatchPipe(toMatch, false, true);
	}
	
	/*
	 * Don't match
	 */
	public static ValueMatchPipe createInverse(String toMatch) {
		return new ValueMatchPipe(toMatch, true, false);
	}
	
	/*
	 * Don't match Ignore Case
	 */
	public static ValueMatchPipe createInverseIgnoreCase(String toMatch) {
		return new ValueMatchPipe(toMatch, true, true);
	}
	
	@Override
	public Block transform(Block block) {
		if(ignoreCase){
			if(inverse){
				//ignore==true, inverse==true
				boolean check = true;
				for(String v :block.fields()) {					
					if(block.value(v).toLowerCase().equals(toMatch.toLowerCase()) ) {
						check = false;
					}
				}
				if(check){
					return block;
				}
			}
			else{
				//ignore==true, inverse==false
				for(String v :block.fields()) {
					if(block.value(v).toLowerCase().equals(toMatch.toLowerCase()) ) {
						return block;
					}
				}
			}
		}else{
			if(inverse){
				//ignore==false, inverse==true
				boolean check = true;
				for(String v :block.fields()) {
					if(block.value(v).equals(toMatch) ) {
						check = false;
					}
				}
				if(check){
					return block;
				}
			}else{
				//ignore==false, inverse==false
				for(String v :block.fields()) {
					if(block.value(v).equals(toMatch) ) {
						return block;
					}
				}
			}		
		}
		return SKIP_BLOCK;
	}

}
