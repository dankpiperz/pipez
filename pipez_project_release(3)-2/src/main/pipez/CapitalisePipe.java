package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

public class CapitalisePipe implements Pipe{

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "CapitalisePipe Character";
	}
	private CapitalisePipe() {}
	@Override
	public Block transform(Block block) {
		// TODO Auto-generated method stub
		SimpleBlock newBlock = new SimpleBlock();
		String[] fields = block.fields();
		
		for(int i=0; i<fields.length; i++) {
			String n=block.value(fields[i]);
			String temp=new String();
			for(int a=0;a<n.length();a++){
				if(Character.isLowerCase(n.charAt(a))){
					temp+=Character.toUpperCase(n.charAt(a));
				}
				else{
					temp+=n.charAt(a);
				}
			}
			newBlock.add(fields[i], temp);
		}
		return newBlock;
	}
	public static CapitalisePipe create() {
		return new CapitalisePipe();
	}
    
}
