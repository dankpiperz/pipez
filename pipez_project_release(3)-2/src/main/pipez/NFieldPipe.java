package pipez;

import java.util.Arrays;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.PipezExceptionHandler;
import pipez.core.SimpleBlock;

/**
 * Selects the n-th field of each block
 * e.g. select the n-th column of a CSV file.
 * 
 *  n=1 corresponds to the first column.
 *  n=-1 corresponds to the last column.
 *  
 * Also allows multiple fields to be selected.
 * 
 * @author whwong
 *
 */
public class NFieldPipe implements Pipe {

	public static NFieldPipe create(int n) {
		return new NFieldPipe(n);
	}

	public static NFieldPipe create(int... ns) {
		return new NFieldPipe(ns);
	}
	
	int[] ns;
	private NFieldPipe(int... ns) {
		this.ns = ns;
	}
	
	@Override
	public String getName() {
		return "n-th field pipe";
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock newBlock = new SimpleBlock();
	    int num=block.fields().length;
	    int[] t=new int[ns.length];
	    int i=0;
	    for(int n:ns){
	    	if(n<0){
	    		 while(n<0 || n>num-1){
	 	    		if(n<0){
	 	    			n=num+n;
	 	    		}
	 	    		else if( n>num-1){
	 	    			n=n-num;
	 	    		} 	    		
	 	    	}
	    		 t[i]=n;
	    	}	    	 
	    	else if(n==0){
	    		 t[i]=-1;
	    	}
	    	else if(n>0){
	    	    n--;
	    		t[i]=n;
	    	}
	    	i++;
	    }
		String[] fields = block.fields();
		for(int n: t) {
			if(n>=0)
				newBlock.add(fields[n], block.value(fields[n]));	
		}
		return newBlock;
	}

}
