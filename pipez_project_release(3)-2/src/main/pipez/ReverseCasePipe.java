package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

public class ReverseCasePipe implements Pipe{

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ReverseCasePipe Character";
	}
	private ReverseCasePipe() {}
	@Override
	public Block transform(Block block) {
		// TODO Auto-generated method stub
		SimpleBlock newBlock = new SimpleBlock();
		String[] values = block.fields();
		
		for(int i=0; i<values.length; i++) {
			String n = block.value(values[i]);
			
			String temp=new String();
			for(int a=0;a<n.length();a++){
				
				if(Character.isLetter(n.charAt(a))){
				
				if(Character.isLowerCase(n.charAt(a))){
					temp+=Character.toUpperCase(n.charAt(a));
					
				}
				else if(Character.isUpperCase(n.charAt(a))){
					temp+=Character.toLowerCase(n.charAt(a));
				}
				else {
					temp+= n.charAt(a);
				}
				
				
				}
				
			}
			newBlock.add(values[i], temp);
		}
		return newBlock;
	}
	public static ReverseCasePipe create() {
		return new ReverseCasePipe();
	}
    
}
