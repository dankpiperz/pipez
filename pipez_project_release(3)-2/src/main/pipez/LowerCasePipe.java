package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

public class LowerCasePipe  implements Pipe{

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "LowercasePipe Character";
	}
	
	private LowerCasePipe(){};
	
	@Override
	public Block transform(Block block) {
		// TODO Auto-generated method stub
		SimpleBlock newblock = new SimpleBlock();
		String[] fields = block.fields();
		
		for(int i=0; i<fields.length; i++) {
			String n = block.value(fields[i]);
			String time = new String();
			for(int j = 0; j < n.length(); j ++){
				if(Character.isUpperCase(n.charAt(j))){
					time += Character.toLowerCase(n.charAt(j));
				}
				else{
					time += n.charAt(j);
				}
			}
			newblock.add(fields[i], time);
		}
		return newblock;
	}
	public static LowerCasePipe create() {
		return new LowerCasePipe();
	
	}

}
