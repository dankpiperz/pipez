package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import pipez.core.SpecialBlocks;

public class ReverseCasePipeTest {
	/* test cases:
	 * 1. Get name
	 * 2. Capitalise letters
	 * 3. Convert to smallercase
	 * 4. Convert a mix of cases
	 * 5. Convert a mix of cases and numbers
	 */
	@Test
	public void test_getname(){
		ReverseCasePipe cp = ReverseCasePipe.create();
		
		assertEquals("ReverseCasePipe Character",cp.getName());
	}
	
	@Test
	public void test_capitalise_letters() {
		ReverseCasePipe cp = ReverseCasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "abc"); 
		sb.add("C2", "def"); 
		sb.add("C3", "ghi");
		sb.add("C4", "jkl");
		Block tb = cp.transform(sb);
		assertThat(tb.value("C1"), is("ABC"));
		assertThat(tb.value("C2"), is("DEF"));
		assertThat(tb.value("C3"), is("GHI"));
		assertThat(tb.value("C4"), is("JKL"));
		
	}
	
	@Test
	public void test_toLowerCase(){
		ReverseCasePipe rp = ReverseCasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1","ABC");
		sb.add("C2","DEF");
		sb.add("C3","GHI");
		Block tb = rp.transform(sb);
		assertThat(tb.value("C1"),is("abc"));

		assertThat(tb.value("C2"),is("def"));

		assertThat(tb.value("C3"),is("ghi"));

	}
	
	@Test
	public void test_toMixedCase(){
		ReverseCasePipe rp = ReverseCasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1","AbC");
		sb.add("C2","dEf");
		sb.add("C3","GhI");
		Block tb = rp.transform(sb);
		assertThat(tb.value("C1"),is("aBc"));

		assertThat(tb.value("C2"),is("DeF"));

		assertThat(tb.value("C3"),is("gHi"));

	}
}
	
	
	