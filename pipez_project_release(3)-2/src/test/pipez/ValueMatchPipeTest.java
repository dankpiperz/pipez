package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class ValueMatchPipeTest {

	//ValueMatchPipe.create()
	@Test
	public void test_getname(){
		ValueMatchPipe vmp = ValueMatchPipe.create("ABC");
		
		
		//String name = ValueMatchPipe.class.getName();
		
		
		assertEquals("Value Match",vmp.getName());
	}
	
	@Test
	public void test_match() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.create("efg");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}
	
	@Test
	public void test_nomatch() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.create("ijk");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test 
	public void test_nomatch_case() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.create("EFG");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	//ValueMatchPipe.createIgnoreCase()
	@Test
	public void test_match_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createIgnoreCase("EFG");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
		
	}
	
	@Test //Do
	public void test_nomatch_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createIgnoreCase("dea");
		
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(0));
	}
	
	//ValueMatchPipe.createInverse()
	@Test //Do
	public void test_match_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi");
		
		ValueMatchPipe select = ValueMatchPipe.createInverse("efg");
		
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(0));
	}

	@Test //Do
	public void test_nomatch_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi");
		
		ValueMatchPipe select = ValueMatchPipe.createInverse("fea");
		
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi"));
	}
	
	//ValueMatchPipe.createInverseIgnoreCase()
	@Test
	public void test_match_inverse_IgnoreCase(){
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi");
		
		ValueMatchPipe select = ValueMatchPipe.createInverseIgnoreCase("CDE");
		
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(0));
	}
	
	@Test
	public void test_nomatch_inverse_IgnoreCase(){
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi");
		
		ValueMatchPipe select = ValueMatchPipe.createInverseIgnoreCase("XYZ");
		
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi"));
	}
}
