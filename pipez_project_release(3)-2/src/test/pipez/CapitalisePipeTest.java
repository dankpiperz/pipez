package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import pipez.core.SpecialBlocks;

public class CapitalisePipeTest {
	
	@Test
	public void test_getname(){
		CapitalisePipe cp = CapitalisePipe.create();
		
		assertEquals("CapitalisePipe Character",cp.getName());
	}
	
	@Test
	public void test_capitalise_pure_letters() {
		CapitalisePipe cp = CapitalisePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "abc"); 
		sb.add("C2", "def"); 
		sb.add("C3", "ghi");
		sb.add("C4", "jkl");
		Block tb = cp.transform(sb);
		assertThat(tb.value("C1"), is("ABC"));
		assertThat(tb.value("C2"), is("DEF"));
		assertThat(tb.value("C3"), is("GHI"));
		assertThat(tb.value("C4"), is("JKL"));
		
	}
	@Test
	public void test_capitalise_letters_mixed_with_numbers() {
		CapitalisePipe cp = CapitalisePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "a1c"); 
		sb.add("C2", "d2f"); 
		sb.add("C3", "g3i");
		sb.add("C4", "j4l");
		Block tb = cp.transform(sb);
		assertThat(tb.value("C1"), is("A1C"));
		assertThat(tb.value("C2"), is("D2F"));
		assertThat(tb.value("C3"), is("G3I"));
		assertThat(tb.value("C4"), is("J4L"));
		
	}
	@Test
	public void test_origin_capital_letter_unchange(){
		CapitalisePipe cp = CapitalisePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "A1c"); 
		sb.add("C2", "ASdF2f"); 
		sb.add("C3", "G3i");
		sb.add("C4", "j4l");
		Block tb = cp.transform(sb);
		assertThat(tb.value("C1"), is("A1C"));
		assertThat(tb.value("C2"), is("ASDF2F"));
		assertThat(tb.value("C3"), is("G3I"));
		assertThat(tb.value("C4"), is("J4L"));
	}
}
