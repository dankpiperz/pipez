package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import pipez.core.SpecialBlocks;

public class LowerCasePipeTest {

	@Test
	public void test_lowercase_pure_letters() {
		LowerCasePipe lc = LowerCasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "DEF"); 
		sb.add("C3", "GHI");
		sb.add("C4", "JKL");
		Block ck = lc.transform(sb);
		assertThat(ck.value("C1"), is("abc"));
		assertThat(ck.value("C2"), is("def"));
		assertThat(ck.value("C3"), is("ghi"));
		assertThat(ck.value("C4"), is("jkl"));
		
	}
	@Test
	public void test_lowercase_letters_mixed_with_numbers() {
		LowerCasePipe lc = LowerCasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "A1C"); 
		sb.add("C2", "D2F"); 
		sb.add("C3", "G3I");
		sb.add("C4", "J4L");
		Block ck = lc.transform(sb);
		assertThat(ck.value("C1"), is("a1c"));
		assertThat(ck.value("C2"), is("d2f"));
		assertThat(ck.value("C3"), is("g3i"));
		assertThat(ck.value("C4"), is("j4l"));
		
	}
	@Test
	public void test_origin_lower_letter_unchange(){
		LowerCasePipe lc = LowerCasePipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "a1C"); 
		sb.add("C2", "asDF2F"); 
		sb.add("C3", "g3I");
		sb.add("C4", "j4L");
		Block ck = lc.transform(sb);
		assertThat(ck.value("C1"), is("a1c"));
		assertThat(ck.value("C2"), is("asdf2f"));
		assertThat(ck.value("C3"), is("g3i"));
		assertThat(ck.value("C4"), is("j4l"));
	}
}
