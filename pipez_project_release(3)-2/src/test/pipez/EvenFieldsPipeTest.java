package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class EvenFieldsPipeTest {

	@Test
	public void test_getname(){
		EvenFieldsPipe efp = EvenFieldsPipe.create();
		
		assertEquals("Even Fields Only",efp.getName());
	}
		
	@Test
	public void test_three_columsn() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first column
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(1));
		assertThat(tb.fields()[0], is("C2"));
	}
	
	@Test
	public void test_four_columns() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first column
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		sb.add("C4", "123"); //fourth column
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C2"));
		assertThat(tb.fields()[1], is("C4"));
	}
	
	@Test
	public void test_0_columns() throws Exception {
		
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(0));
	}
}
