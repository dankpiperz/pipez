package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import sun.font.CreatedFontTracker;

import static pipez.util.TestUtils.*;

public class FieldMatchPipeTest {

	//FieldMatchPipe.create()
	
	@Test
	public void test_getname(){
		FieldMatchPipe fmp = FieldMatchPipe.create("ABC");
		
		assertEquals("Field Match",fmp.getName());
	}
	
	
	
	
	@Test
	public void test_match() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("efg");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}
	
	@Test
	public void test_nomatch() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("xyz");
		Block b = select.transform(sb);
		assertThat(b.values().length, is(0));

	}
	
	@Test public void test_nomatch_case() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("ABC");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}

	//FieldMatchPipe.createIgnoreCase()
	@Test
	public void test_match_ignorecase() { // done
		SimpleBlock sb = new SimpleBlock("abc","cde","efg","ghi");
		
		FieldMatchPipe select = FieldMatchPipe.createIgnoreCase("ABC");
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(4));
		assertThat(b.values(),are("abc","cde","efg","ghi"));
	}
	
	@Test
	public void test_nomatch_ignorecase() {  
		SimpleBlock sb = new SimpleBlock("abc","cde","efg","ghi");
		
		FieldMatchPipe select = FieldMatchPipe.createIgnoreCase("XYZ");
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(0));	
	}
	
	//FieldMatchPipe.createInverse()
	@Test
	public void test_match_inverse() { 
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse("efg");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}

	@Test
	public void test_nomatch_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse("xyz");
		Block b = select.transform(sb);
		
		assertThat(b.values().length, is(4));
		assertThat(b.values(),are("abc","cde","efg","ghi"));
	}
	
	//FieldMatchPipe.createInverseIgnoreCase()
	//inverse= true and ignore case = true
	
	@Test
	
	public void test_match_inverse_ignorecase(){
		SimpleBlock Sb = new SimpleBlock("abc","cde","efg","ghi");
		
		FieldMatchPipe select = FieldMatchPipe.createInverseIgnoreCase("ABC");
		Block b = select.transform(Sb);
		//assertNull(b.values());
		
		assertThat(b.values().length, is (0));
		
	}

	@Test
	public void test_nomatch_inverse_ignorecase(){
		SimpleBlock Sb = new SimpleBlock("abc","cde","efg","ghi");
		
		FieldMatchPipe select = FieldMatchPipe.createInverseIgnoreCase("XYZ");
		Block b = select.transform(Sb);
		
		assertThat(b.values().length, is(4));
		assertThat(b.values(),are("abc","cde","efg","ghi"));
		
	}
	
	

}
